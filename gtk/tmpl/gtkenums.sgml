<!-- ##### SECTION Title ##### -->
Standard Enumerations

<!-- ##### SECTION Short_Description ##### -->
Public enumerated types used throughout GTK+.

<!-- ##### SECTION Long_Description ##### -->
<para>

</para>

<!-- ##### SECTION See_Also ##### -->
<para>

</para>

<!-- ##### ENUM GtkAccelFlags ##### -->
<para>

</para>

@GTK_ACCEL_VISIBLE: 
@GTK_ACCEL_SIGNAL_VISIBLE: 
@GTK_ACCEL_LOCKED: 
@GTK_ACCEL_MASK: 

<!-- ##### ENUM GtkArrowType ##### -->
<para>
Used to indicate the direction in which a #GtkArrow should point.
</para>

@GTK_ARROW_UP: Represents an upward pointing arrow.
@GTK_ARROW_DOWN: Represents a downward pointing arrow.
@GTK_ARROW_LEFT: Represents a left pointing arrow.
@GTK_ARROW_RIGHT: Represents a right pointing arrow.

<!-- ##### ENUM GtkAttachOptions ##### -->
<para>
Denotes the expansion properties that a widget will have when it (or it's
parent) is resized.
</para>

@GTK_EXPAND: the widget should expand to take up any extra space in its
container that has been allocated.
@GTK_SHRINK: the widget should shrink as and when possible.
@GTK_FILL: the widget should fill the space allocated to it.

<!-- ##### ENUM GtkButtonBoxStyle ##### -->
<para>
Used to dictate the style that a #GtkButtonBox uses to layout the buttons it
contains. (See also: #GtkVButtonBox and #GtkHButtonBox).
</para>

@GTK_BUTTONBOX_DEFAULT_STYLE: Default packing.
@GTK_BUTTONBOX_SPREAD: Buttons are evenly spread across the ButtonBox.
@GTK_BUTTONBOX_EDGE: Buttons are placed at the edges of the ButtonBox.
@GTK_BUTTONBOX_START: Buttons are grouped towards the start of box, (on the
left for a HBox, or the top for a VBox).
@GTK_BUTTONBOX_END: Buttons are grouped towards the end of a box, (on the
right for a HBox, or the bottom for a VBox).

<!-- ##### ENUM GtkCornerType ##### -->
<para>
Specifies which corner a child widget should be placed in when packed into
a #GtkScrolledWindow. This is effectively the opposite of where the scroll
bars are placed.
</para>

@GTK_CORNER_TOP_LEFT: Place the scrollbars on the right and bottom of the
widget (default behaviour).
@GTK_CORNER_BOTTOM_LEFT: Place the scrollbars on the top and right of the
widget.
@GTK_CORNER_TOP_RIGHT: Place the scrollbars on the left and bottom of the
widget.
@GTK_CORNER_BOTTOM_RIGHT: Place the scrollbars on the top and left of the
widget.

<!-- ##### ENUM GtkCurveType ##### -->
<para>

</para>

@GTK_CURVE_TYPE_LINEAR: 
@GTK_CURVE_TYPE_SPLINE: 
@GTK_CURVE_TYPE_FREE: 

<!-- ##### ENUM GtkDirectionType ##### -->
<para>

</para>

@GTK_DIR_TAB_FORWARD: 
@GTK_DIR_TAB_BACKWARD: 
@GTK_DIR_UP: 
@GTK_DIR_DOWN: 
@GTK_DIR_LEFT: 
@GTK_DIR_RIGHT: 

<!-- ##### ENUM GtkJustification ##### -->
<para>
Used for justifying the text inside a #GtkLabel widget. (See also
#GtkAlignment).
</para>

@GTK_JUSTIFY_LEFT: The text is placed at the left edge of the label.
@GTK_JUSTIFY_RIGHT: The text is placed at the right edge of the label.
@GTK_JUSTIFY_CENTER: The text is placed in the center of the label.
@GTK_JUSTIFY_FILL: The text is placed is distributed across the label.

<!-- ##### ENUM GtkMatchType ##### -->
<para>

</para>

@GTK_MATCH_ALL: 
@GTK_MATCH_ALL_TAIL: 
@GTK_MATCH_HEAD: 
@GTK_MATCH_TAIL: 
@GTK_MATCH_EXACT: 
@GTK_MATCH_LAST: 

<!-- ##### ENUM GtkMetricType ##### -->
<para>

</para>

@GTK_PIXELS: 
@GTK_INCHES: 
@GTK_CENTIMETERS: 

<!-- ##### ENUM GtkOrientation ##### -->
<para>

</para>

@GTK_ORIENTATION_HORIZONTAL: 
@GTK_ORIENTATION_VERTICAL: 

<!-- ##### ENUM GtkPackType ##### -->
<para>
Used for packing widgets into #GtkBox widgets. (See also: #GtkVBox,
#GtkHBox, and #GtkButtonBox).
</para>

@GTK_PACK_START: The 
@GTK_PACK_END: 

<!-- ##### ENUM GtkPathPriorityType ##### -->
<para>

</para>

@GTK_PATH_PRIO_LOWEST: 
@GTK_PATH_PRIO_GTK: 
@GTK_PATH_PRIO_APPLICATION: 
@GTK_PATH_PRIO_RC: 
@GTK_PATH_PRIO_HIGHEST: 
@GTK_PATH_PRIO_MASK: 

<!-- ##### ENUM GtkPathType ##### -->
<para>

</para>

@GTK_PATH_WIDGET: 
@GTK_PATH_WIDGET_CLASS: 
@GTK_PATH_CLASS: 

<!-- ##### ENUM GtkPolicyType ##### -->
<para>
Determines when a scroll bar will be visible. 
</para>

@GTK_POLICY_ALWAYS: The scrollbar is always visible.
@GTK_POLICY_AUTOMATIC: The scrollbar will appear and disappear as necessary. For example,
when all of a #GtkCList can not be seen.
@GTK_POLICY_NEVER: The scrollbar will never appear.

<!-- ##### ENUM GtkPositionType ##### -->
<para>

</para>

@GTK_POS_LEFT: 
@GTK_POS_RIGHT: 
@GTK_POS_TOP: 
@GTK_POS_BOTTOM: 

<!-- ##### ENUM GtkPreviewType ##### -->
<para>
An enumeration which describes whether a preview
contains grayscale or red-green-blue data.
</para>

@GTK_PREVIEW_COLOR: the preview contains red-green-blue data.
@GTK_PREVIEW_GRAYSCALE: The preview contains grayscale data.

<!-- ##### ENUM GtkReliefStyle ##### -->
<para>

</para>

@GTK_RELIEF_NORMAL: 
@GTK_RELIEF_HALF: 
@GTK_RELIEF_NONE: 

<!-- ##### ENUM GtkResizeMode ##### -->
<para>

</para>

@GTK_RESIZE_PARENT: 
@GTK_RESIZE_QUEUE: 
@GTK_RESIZE_IMMEDIATE: 

<!-- ##### ENUM GtkScrollType ##### -->
<para>

</para>

@GTK_SCROLL_NONE: 
@GTK_SCROLL_STEP_BACKWARD: 
@GTK_SCROLL_STEP_FORWARD: 
@GTK_SCROLL_PAGE_BACKWARD: 
@GTK_SCROLL_PAGE_FORWARD: 
@GTK_SCROLL_JUMP: 

<!-- ##### ENUM GtkSelectionMode ##### -->
<para>

</para>

@GTK_SELECTION_SINGLE: 
@GTK_SELECTION_BROWSE: 
@GTK_SELECTION_MULTIPLE: 
@GTK_SELECTION_EXTENDED: 

<!-- ##### ENUM GtkShadowType ##### -->
<para>
Used to change the appearance of an outline typically provided by a #GtkFrame.
</para>

@GTK_SHADOW_NONE: No outline.
@GTK_SHADOW_IN: The outline is bevelled inwards.
@GTK_SHADOW_OUT: The outline is bevelled outwards like a button.
@GTK_SHADOW_ETCHED_IN: The outline itself is an inward bevel, but the frame
does 
@GTK_SHADOW_ETCHED_OUT: 

<!-- ##### ENUM GtkStateType ##### -->
<para>
This type indicates the current state of a widget.
</para>

@GTK_STATE_NORMAL: The state during normal operation.
@GTK_STATE_ACTIVE: The widget is currently active, such as a
@GTK_STATE_PRELIGHT: The mouse pointer is over the widget.
@GTK_STATE_SELECTED: 
@GTK_STATE_INSENSITIVE: The state of the widget can not be altered by the
user. Its appearance will usually indicate this.

<!-- ##### ENUM GtkSubmenuDirection ##### -->
<para>
Indicates the direction a sub-menu will appear.
</para>

@GTK_DIRECTION_LEFT: A sub-menu will appear
@GTK_DIRECTION_RIGHT: 

<!-- ##### ENUM GtkSubmenuPlacement ##### -->
<para>

</para>

@GTK_TOP_BOTTOM: 
@GTK_LEFT_RIGHT: 

<!-- ##### ENUM GtkToolbarStyle ##### -->
<para>
Used to customize the appearance of a #GtkToolbar.
</para>

@GTK_TOOLBAR_ICONS: Buttons should display only icons in the toolbar.
@GTK_TOOLBAR_TEXT: Buttons should display only text labels in the toolbar.
@GTK_TOOLBAR_BOTH: Buttons should display text and icons in the toolbar.

<!-- ##### ENUM GtkTroughType ##### -->
<para>

</para>

@GTK_TROUGH_NONE: 
@GTK_TROUGH_START: 
@GTK_TROUGH_END: 
@GTK_TROUGH_JUMP: 

<!-- ##### ENUM GtkUpdateType ##### -->
<para>

</para>

@GTK_UPDATE_CONTINUOUS: 
@GTK_UPDATE_DISCONTINUOUS: 
@GTK_UPDATE_DELAYED: 

<!-- ##### ENUM GtkVisibility ##### -->
<para>

</para>

@GTK_VISIBILITY_NONE: 
@GTK_VISIBILITY_PARTIAL: 
@GTK_VISIBILITY_FULL: 

<!-- ##### ENUM GtkWindowPosition ##### -->
<para>
Window placement can be influenced using this enumeration.
</para>

@GTK_WIN_POS_NONE: No influence is made on placement.
@GTK_WIN_POS_CENTER: Windows should be placed in the center of the screen.
@GTK_WIN_POS_MOUSE: Windows should be placed at the current mouse position.

<!-- ##### ENUM GtkWindowType ##### -->
<para>
A #GtkWindow can be of these types. A toplevel window has standard window
decorations by default. A dialog may have fewer decorations and obey a
different placement policy. A popup window will have no decorations.
</para>
<para>
It should be noted that such decorations can often be over-ridden by modern
window managers.
</para>

@GTK_WINDOW_TOPLEVEL: A window for a typical application.
@GTK_WINDOW_DIALOG: A window for transient messages and dialogs.
@GTK_WINDOW_POPUP: A window for popups.

<!-- ##### ENUM GtkSortType ##### -->
<para>
Determines the direction of a sort.
</para>

@GTK_SORT_ASCENDING: Sorting is in ascending order.
@GTK_SORT_DESCENDING: Sorting is in descending order.

