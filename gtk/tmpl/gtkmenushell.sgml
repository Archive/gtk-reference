<!-- ##### SECTION Title ##### -->
GtkMenuShell

<!-- ##### SECTION Short_Description ##### -->
a base class for menu objects.

<!-- ##### SECTION Long_Description ##### -->
<para>
A #GtkMenuShell is the abstract base class used to derive the 
#GtkMenu and #GtkMenuBar subclasses.  
</para>

<para>
A #GtkMenuShell is a container of #GtkMenuItem objects arranged in a 
list which can be navigated, selected, and activated by the user to perform
application functions.  A #GtkMenuItem can have a submenu associated with it,
allowing for nested hierarchical menus.
</para>

<!-- ##### SECTION See_Also ##### -->
<para>

</para>

<!-- ##### STRUCT GtkMenuShell ##### -->
<para>
The #GtkMenuShell-struct struct contains the following fields.
(These fields should be considered read-only. They should never be set by
an application.)

<informaltable pgwide=1 frame="none" role="struct">
<tgroup cols="2"><colspec colwidth="2*"><colspec colwidth="8*">
<tbody>

<row>
<entry>#GList *children;</entry>
<entry>The list of #GtkMenuItem objects contained by this #GtkMenuShell.
</entry>
</row>
</tbody></tgroup></informaltable>
</para>


<!-- ##### FUNCTION gtk_menu_shell_append ##### -->
<para>
Adds a new #GtkMenuItem to the end of the menu shell's item list.
</para>

@menu_shell: a #GtkMenuShell.
@child: The #GtkMenuItem to add.


<!-- ##### FUNCTION gtk_menu_shell_prepend ##### -->
<para>
Adds a new #GtkMenuItem to the beginning of the menu shell's item list.
</para>

@menu_shell: a #GtkMenuShell.
@child: The #GtkMenuItem to add.


<!-- ##### FUNCTION gtk_menu_shell_insert ##### -->
<para>
Adds a new #GtkMenuItem to the menu shell's item list at the position
indicated by @position. 
</para>

@menu_shell: a #GtkMenuShell.
@child: The #GtkMenuItem to add.
@position: The position in the item list where @child is added.
Positions are numbered from 0 to n-1.


<!-- ##### FUNCTION gtk_menu_shell_deactivate ##### -->
<para>
Deactivates the menu shell.  Typically this results in the menu shell
being erased from the screen.
</para>

@menu_shell: a #GtkMenuShell.


<!-- ##### FUNCTION gtk_menu_shell_select_item ##### -->
<para>
Selects the menu item from the menu shell.
</para>

@menu_shell: a #GtkMenuShell.
@menu_item: The #GtkMenuItem to select.


<!-- ##### FUNCTION gtk_menu_shell_activate_item ##### -->
<para>
Activates the menu item within the menu shell.
</para>

@menu_shell: a #GtkMenuShell.
@menu_item: The #GtkMenuItem to activate.
@force_deactivate: If TRUE, force the deactivation of the menu shell
after the menu item is activated.


<!-- ##### ENUM GtkMenuDirectionType ##### -->
<para>
An enumeration representing directional movements within a menu.

<informaltable pgwide=1 frame="none" role="enum">
<tgroup cols="2"><colspec colwidth="2*"><colspec colwidth="8*">
<tbody>

<row>
<entry>GTK_MENU_DIR_PARENT</entry>
<entry>To the parent menu shell.</entry>
</row>

<row>
<entry>GTK_MENU_DIR_CHILD</entry>
<entry>To the submenu, if any, associated with the item.</entry>
</row>

<row>
<entry>GTK_MENU_DIR_NEXT</entry>
<entry>To the next menu item.</entry>
</row>

<row>
<entry>GTK_MENU_DIR_PREV</entry>
<entry>To the previous menu item.</entry>
</row>

</tbody></tgroup></informaltable>
</para>

@GTK_MENU_DIR_PARENT: 
@GTK_MENU_DIR_CHILD: 
@GTK_MENU_DIR_NEXT: 
@GTK_MENU_DIR_PREV: 

<!-- ##### SIGNAL GtkMenuShell::deactivate ##### -->
<para>
This signal is emitted when a menu shell is deactivated.
</para>

@menushell: the object which received the signal.

<!-- ##### SIGNAL GtkMenuShell::selection-done ##### -->
<para>
This signal is emitted when a selection has been completed within a menu
shell.
</para>

@menushell: the object which received the signal.

<!-- ##### SIGNAL GtkMenuShell::move-current ##### -->
<para>
An action signal which moves the current menu item in the direction 
specified by @direction.
</para>

@menushell: the object which received the signal.
@direction: the direction to move.

<!-- ##### SIGNAL GtkMenuShell::activate-current ##### -->
<para>
An action signal that activates the current menu item within the menu
shell.
</para>

@menushell: the object which received the signal.
@force_hide: if TRUE, hide the menu after activating the menu item.

<!-- ##### SIGNAL GtkMenuShell::cancel ##### -->
<para>
An action signal which cancels the selection within the menu shell.
Causes the GtkMenuShell::selection-done signal to be emitted.
</para>

@menushell: the object which received the signal.

