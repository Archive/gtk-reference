<!-- ##### SECTION Title ##### -->
GtkVButtonBox

<!-- ##### SECTION Short_Description ##### -->
a container for arranging buttons vertically.

<!-- ##### SECTION Long_Description ##### -->
<para>
A button box should be used to provide a consistent layout of buttons
throughout your application. There is one default layout and a default
spacing value that are persistant across all #VButtonBox widgets.
</para>
<para>
The layout/spacing can then be altered by the programmer, or if desired, by
the user to alter the 'feel' of a program to a small degree.
</para>
<para>
A #VButtonBox is created with gtk_vbutton_box_new(). Buttons are packed into
a button box the same way as any other box, using gtk_box_pack_start() or
gtk_box_pack_end().
</para>
<para>
The default spacing between buttons can be set with
gtk_vbutton_box_set_spacing_default() and queried with
gtk_vbutton_box_get_spacing_default().
</para>
<para>
The arrangement and layout of the buttons can be changed using
gtk_vbutton_box_set_layout_default() and queried with
gtk_vbutton_box_get_layout_default().
</para>

<!-- ##### SECTION See_Also ##### -->
<para>
<variablelist>
<varlistentry>
<term>#GtkBox</term>
<listitem><para>Used to pack widgets into button boxes.</para></listitem>
</varlistentry><varlistentry>
<term>#GtkButtonBox</term>
<listitem><para>Provides functions for controlling button boxes.</para></listitem>
</varlistentry>
<varlistentry>
<term>#GtkHButtonBox</term>
<listitem><para>Pack buttons horizontally.</para></listitem>
</varlistentry>
</variablelist>
</para>

<!-- ##### STRUCT GtkVButtonBox ##### -->
<para>

</para>

@button_box: the #GtkButtonBox that this class is derived from.

<!-- ##### FUNCTION gtk_vbutton_box_new ##### -->
<para>
Creates a new vertical button box.
</para>

@Returns: a new button box #GtkWidget.


<!-- ##### FUNCTION gtk_vbutton_box_get_spacing_default ##### -->
<para>
Retrieves the current default spacing for vertical button boxes. This is the number of pixels 
to be placed between the buttons when they are arranged.
</para>

@Returns: the default number of pixels between buttons.


<!-- ##### FUNCTION gtk_vbutton_box_set_spacing_default ##### -->
<para>
Changes the default spacing that is placed between widgets in an
vertical button box.
</para>

@spacing: an integer value.


<!-- ##### FUNCTION gtk_vbutton_box_get_layout_default ##### -->
<para>
Retrieves the current layout used to arrange buttons in button box widgets.
</para>

@Returns: the current #GtkButtonBoxStyle.


<!-- ##### FUNCTION gtk_vbutton_box_set_layout_default ##### -->
<para>
Sets a new layout mode that will be used by all button boxes.
</para>

@layout: a new #GtkButtonBoxStyle.
