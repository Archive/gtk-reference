<!-- ##### SECTION Title ##### -->
Implementation of Object Properties

<!-- ##### SECTION Short_Description ##### -->
Utility function to manipulate lists of named, typed arguments.

<!-- ##### SECTION Long_Description ##### -->
<para>
All the functions in here are marked a Non-public.
We describe it anyway because it is occasionally useful
to understand how the work is done.
</para>
<para>
Arguments are a way of describing a named parameter to a function.
They have two important roles within gtk+:
<itemizedlist>
<listitem>
<para>
they describe <wordasword>object properties</wordasword>.
This means that they present an interface to get and set a named-type
for any type of object in a consistent way.
(All the relevant functions to do this start with gtk_object_set
or gtk_object_get).
</para>
</listitem>
<listitem>
<para>
they describe <wordasword>signal arguments</wordasword>.
This is a lot less often needed but still useful.
Usually if you are just emitting or creating a particular signal
it is more convenient to just use gtk_signal_emit() or gtk_signal_new().
However if you are writing a function to emit or create an arbitrary
signal, you must use gtk_signal_emitv() or gtk_signal_newv().
</para>
</listitem>
</itemizedlist>
</para>

<!-- ##### SECTION See_Also ##### -->
<para>
#GtkObject.
</para>

<!-- ##### STRUCT GtkArgInfo ##### -->
<para>
A structure containing information about the argument.
Returned by gtk_arg_get_info().
</para>

@class_type: if the argument is an object, this is the object class type.
@name: the name of the argument.
@type: the type of the argument; it may be an object's type
or a fundamental type.
@arg_flags: flags applicable to the argument (i.e. readable, writable,
and whether it needs to be constructed).
@full_name: the object name and argument name separated by ::,
e.g. "GtkObject::user_data" or "GtkButton::label".
@arg_id: the unique argument identified.
@seq_id: ???

<!-- ##### FUNCTION gtk_arg_new ##### -->
<para>
Creates a new argument of a certain type, set to 0 or NULL.
</para>

@arg_type: the type of the argument.
@Returns: the newly created #GtkArg.


<!-- ##### FUNCTION gtk_arg_copy ##### -->
<para>
It will either copy data into an existing argument or allocate a new argument
and copy the data.  Strings are duplicated.  All other pointers and
values are copied (shallowly-- that is the pointers themselves are
copied, not the data they point to.)
</para>
<para>
You should call gtk_arg_reset() on dest_arg before calling this
if the argument may contain string data that you want freed.
</para>

@src_arg: the argument to duplicate.
@dest_arg: the argument to copy over (or NULL to create a new #GtkArg).
@Returns: the new #GtkArg (or dest_arg, if it was not NULL).


<!-- ##### FUNCTION gtk_arg_free ##### -->
<para>
Frees the argument, and optionally its contents.
</para>

@arg: the argument to free.
@free_contents: whether to free the string, if it is a string.


<!-- ##### FUNCTION gtk_args_collect ##### -->
<para>
Private:  given a hashtable of argument information it takes a vararg
list and parses it into arguments (in the form of lists of #GtkArgs
and lists of #GtkArgInfos.
</para>
<para>
The list of arguments starts with first_arg_name then the first argument's
value.  Followed by any number of additional name/argument pairs,
terminated with NULL.
</para>

@object_type: the type of object we are collecting arguments for.
@arg_info_hash_table: a hashtable mapping from names of arguments
to their #GtkArgInfos.
@arg_list_p: a returned list of arguments obtained from parsing the
varargs.
@info_list_p: a returned list of the #GtkArgInfos.
@first_arg_name: the name of the first argument.
@var_args: a va_list containing the value of the first argument,
followed by name/value pairs, followed by NULL.
@Returns: an error message on failure, or NULL otherwise.


<!-- ##### FUNCTION gtk_args_collect_cleanup ##### -->
<para>
Private: erase lists of arguments returned from gtk_args_collect().
</para>

@arg_list: arg_list_p returned from gtk_args_collect().
@info_list: info_list_p returned from gtk_args_collect().


<!-- ##### FUNCTION gtk_arg_get_info ##### -->
<para>
Private: get information about an argument.
</para>

@object_type: the type of object.
@arg_info_hash_table: the hashtable of #GtkArgInfos.
@arg_name: the name of the argument to lookup.
@info_p: the argument info.
@Returns: an error message on failure, or NULL otherwise.


<!-- ##### FUNCTION gtk_arg_type_new_static ##### -->
<para>
Create a new argument registered with a class.
</para>

@base_class_type: the basic type having the arguments, almost alway
GTK_TYPE_OBJECT, except if your defining a different type argument
that gets a different namespace.  #GtkContainer does this to define
per-child arguments of the container.
@arg_name: name of the argument to create.  (must be a static constant string)
@class_n_args_offset: offset into the base class structure that tells
the number of arguments.
@arg_info_hash_table: hashtable of #GtkArgInfos.
@arg_type: type of the argument.
@arg_flags: flags of the argument.
@arg_id: ???
@Returns: the new #GtkArgInfo.


<!-- ##### FUNCTION gtk_args_query ##### -->
<para>
Private: from a class type and its arginfo hashtable,
get an array of #GtkArgs that this object accepts.
</para>

@class_type: the class type.
@arg_info_hash_table: the hashtable of #GtkArgInfos.
@arg_flags: returned array of argument flags.
@n_args_p: the number of arguments this object accepts.
@Returns: the array of arguments (or NULL on error).


<!-- ##### FUNCTION gtk_arg_name_strip_type ##### -->
<para>
Given a fully qualified argument name (e.g. "GtkButton::label")
it returns just the argument name (e.g. "label") unless
the argument name was invalid, in which case it returns NULL.
</para>

@arg_name: the fully-qualified argument name.
@Returns: the base argument name.


<!-- ##### FUNCTION gtk_arg_info_equal ##### -->
<para>
A #GCompareFunc for hashing #GtkArgInfos.
</para>

@arg_info_1: a #GtkArgInfo.
@arg_info_2: a #GtkArgInfo.
@Returns: whether the arguments are the same.


<!-- ##### FUNCTION gtk_arg_info_hash ##### -->
<para>
A #GHashFunc for hashing #GtkArgInfos.
</para>

@arg_info: a #GtkArgInfo.
@Returns: a hash value for that #GtkArgInfo.


